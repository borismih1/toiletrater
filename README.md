# Toiletrater

This project is the back-end web server of the future application Toiletrater. The goal of this application will be rating the toilet/bathroom services in various restourants, bars, nightclubs and such. It will be simillar to `Tripadvisor`. The project was written in Typescript using the NestJS web framework.

In order to access the documentation and code, please visit the `develop` branch of this project.
